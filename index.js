var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var _ = require('lodash');

var people = {};

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/index.html');
})

io.on('connection', function(socket){
	io.emit('no of users', io.engine.clientsCount);

	socket.on('disconnect', function(){
		delete people[socket.id];
		io.emit('people list', people);
	});

	socket.on('join', function(userName) {
		people[socket.id] = userName;
		io.emit('chat message', userName + ' has joined the chat');
		io.emit('people list', people);
	});

	socket.on('chat message', function(msg, user) {
		io.emit('chat message', user + ': ' + msg);
	})

	socket.on('pm message', function(userid, pmMsg, pmFrom) {
		io.to(userid).emit('chat message', 'From ' + pmFrom + ': '+ pmMsg);
	})
});



http.listen(3500, function() {
	console.log('listening on *:3500');
})
