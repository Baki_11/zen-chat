A simple NodeJS chat based on socket.io (AngularJS on the front).

# Instalation
1) You need to have NodeJS installed.
2) Run from the root folder:

	npm install

3) Start the server by:

	node index.js

4) Go to:

	localhost:3500/

That's all
